package com.hyperiondev;

import java.util.Date;

public class Account {
	private int id;

	private double balance;

	private Date dateCreated;

	protected Account(int id, double balance) {
		this.id = id;
		this.balance = balance;
		this.dateCreated = new Date();
	}

	protected void withdraw(double amount) {
		double newBalance = balance - amount;
		setBalance(newBalance);
	}

	protected void deposit(double amount) {
		double newBalance = balance + amount;
		setBalance(newBalance);
	}

	public int getId() {
		return id;
	}

	public double getBalance() {
		return balance;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

}
