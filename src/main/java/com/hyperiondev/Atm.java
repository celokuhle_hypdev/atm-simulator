package com.hyperiondev;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

public class Atm {

	public static void main(String[] args) {
		List<Account> accounts = getAccounts();
		
		while (true) {
			double id = input("Enter Account ID");
			Account account = getAccount(id, accounts);
			if (account == null) {
				output("Invalid account number entered");
			} else {
				handleMainMenu(account);
			}
		}
	}

	private static void handleMainMenu(Account account) {
		boolean exit = false;
		while (!exit) {
			int option = (int) input("Main menu\r\n 1. check the balance\r\n 2. withdraw\r\n 3. deposit\r\n 4. exit");
			switch (option) {
			case 1: // check balance
			{
				account.getBalance();
				output("Balance: " + account.getBalance());
				break;
			}
			case 2: // withdraw
			{
				double amount = input("Amount to withdraw:");
				account.withdraw(amount);
				output("Balance: " + account.getBalance());
				break;
			}
			case 3: // deposit
			{
				double amount = input("Amount to deposit:");
				account.deposit(amount);
				output("Balance: " + account.getBalance());
				break;
			}
			case 4: // exit
			{
				exit = true;
				break;
			}
			default:
				output("Invalid option");
				break;
			}
		}
	}

	private static Account getAccount(double id, List<Account> accounts) {
		for (Account account : accounts) {
			if (account.getId() == id) {
				return account;
			}
		}
		return null;
	}

	private static void output(String message) {
		JOptionPane.showMessageDialog(null, message);
	}

	private static double input(String message) {
		double input = -1;
		String originalMessage = message;
		do {
			try {
				String inputString = JOptionPane.showInputDialog(message);
				if (inputString == null) {
					throw new NumberFormatException();
				}

				input = Double.parseDouble(inputString);
			} catch (NumberFormatException e) {
				message = "Invalid input. Try gain\r\n".concat(originalMessage);
			}
		} while (input == -1);
		return input;
	}

	private static List<Account> getAccounts() {
		List<Account> accounts = new ArrayList<>();

		// savings accounts
		accounts.add(new SavingsAccount(1001, 10000.00));
		accounts.add(new SavingsAccount(1002, 500.50));
		accounts.add(new SavingsAccount(1003, 1500.99));
		accounts.add(new SavingsAccount(1004, 1400.85));
		accounts.add(new SavingsAccount(1005, 50000.8));

		// checking accounts
		accounts.add(new CheckingAccount(1006, 2000.22));
		accounts.add(new CheckingAccount(1007, 4500.22));
		accounts.add(new CheckingAccount(1008, 100000.56));
		accounts.add(new CheckingAccount(1009, 50032.33));
		accounts.add(new CheckingAccount(10010, 80000.99));

		return accounts;
	}
}
