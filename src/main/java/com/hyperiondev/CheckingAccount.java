package com.hyperiondev;

public class CheckingAccount extends Account {

	private double overdraft;

	public CheckingAccount(int id, double balance) {
		super(id, balance);
	}

	public double getOverdraft() {
		return overdraft;
	}

}
