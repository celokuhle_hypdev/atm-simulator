package com.hyperiondev;

public class SavingsAccount extends Account {

	private static final int NUM_OF_MONTHS = 12;

	private double annualInterestRate;

	public SavingsAccount(int id, double balance) {
		super(id, balance);
	}

	public double getMonthlyInterestRate() {
		return (annualInterestRate / 100) / NUM_OF_MONTHS;
	}

	public double getMonthlyInterest() {
		return getBalance() * getMonthlyInterestRate();
	}

	//for testing purposes
	public void setAnnualInterestRate(double annualInterestRate) {
		this.annualInterestRate = annualInterestRate;
	}
	

}
