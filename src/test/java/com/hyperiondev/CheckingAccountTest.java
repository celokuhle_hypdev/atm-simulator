package com.hyperiondev;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class CheckingAccountTest {

	private Account account;

	@Before
	public void setUp() {
		account = new CheckingAccount(200, 1000);
	}
	
	@Test
	public void testCheckingAccountCanDeposit() {
		account.deposit(500);
		assertEquals(1500, account.getBalance(), 0.1);
	}
	
	@Test
	public void testCheckingAccountCanWithdraw() {
		account.withdraw(500);
		assertEquals(500, account.getBalance(), 0.1);
	}

}
