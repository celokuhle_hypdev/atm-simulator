package com.hyperiondev;

import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.Before;

public class SavingsAccountTest {
	private Account account;

	@Before
	public void setUp() {
		account = new SavingsAccount(100, 1000);
	}

	@Test
	public void testSavingsAccountCanDeposit() {
		account.deposit(200);
		assertEquals(1200.00, account.getBalance(), 0.1);
	}

	@Test
	public void testSavingsAccountCanWithdraw() {
		account.withdraw(200);
		assertEquals(800.00, account.getBalance(), 0.1);
	}

	@Test
	public void testGetMonthlyInterestRate() {
		SavingsAccount savingsAccount = (SavingsAccount) account;
		savingsAccount.setAnnualInterestRate(12.00);

		assertEquals(0.01, savingsAccount.getMonthlyInterestRate(), 0.1);
	}

	@Test
	public void testGetMonthlyInterest() {
		SavingsAccount savingsAccount = (SavingsAccount) account;
		savingsAccount.setAnnualInterestRate(12.00);

		assertEquals(10, savingsAccount.getMonthlyInterest(), 0.1);
	}

}
